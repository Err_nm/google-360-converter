package main

import (
	"crypto/md5"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

func upload(w http.ResponseWriter, r *http.Request) {
	fmt.Println("method:", r.Method)
	if r.Method == "GET" {
		crutime := time.Now().Unix()
		h := md5.New()
		io.WriteString(h, strconv.FormatInt(crutime, 10))
		token := fmt.Sprintf("%x", h.Sum(nil))

		t, _ := template.ParseFiles("./templates/upload.gtpl")
		t.Execute(w, token)
	} else {
		r.ParseMultipartForm(32 << 20)
		file, handler, err := r.FormFile("uploadfile")
		if err != nil {
			fmt.Println(err)
			return
		}
		defer file.Close()
		//fmt.Fprintf(w, "%v", handler.Header)
		f, err := os.OpenFile("./test/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			fmt.Println(err)
			return
		}
		defer f.Close()
		io.Copy(f, file)

		filename := string(handler.Filename)

		files, err := Unzip("./test/"+filename, "workdir")
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Unzipped:\n" + strings.Join(files, "+++\n"))

		filenames := Ls("./workdir/")
		fmt.Println(filenames)

		for _, file := range filenames {
			filepath := "./workdir/" + file
			dir := strings.Replace(file, ".zip", "", 1)
			dpath := "./download/" + dir

			if file != "__MACOSX" {
				_, err := Unzip(filepath, dpath)
				if err != nil {
					log.Fatal(err)
				}
				Converse(dpath)
				ZipWriter(dpath, "./finalfiles/"+file)
			}
		}

		ZipWriter("./finalfiles", "360.zip")

		http.Redirect(w, r, "/download", http.StatusFound)

		RemoveContents("./workdir/")
		RemoveContents("./download/")
		RemoveContents("./test/")
		RemoveContents("./finalfiles/")

	}
}

func download(w http.ResponseWriter, r *http.Request) {
	filename := "./360.zip"
	w.Header().Set("Content-Disposition", "attachment; filename=360.zip")
	w.Header().Set("Content-Type", "application/octet-stream")
	http.ServeFile(w, r, filename)

}

func main() {
	http.HandleFunc("/", upload)
	http.HandleFunc("/download", download)

	err := http.ListenAndServe(":9090", nil) // setting listening port
	fmt.Println("Server listen por 9090")
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
