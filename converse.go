package main

import (
    "io/ioutil"
    "log"
    "os"
    "regexp"
    "strings"
)

func Converse(dir string) {
    dir = strings.Replace(dir, ".zip", "", 1)

    index := dir + "/index.html"

    // Read entire file content, giving us little control but
    // making it very simple. No need to close the file.
    content, err := ioutil.ReadFile(index)
    if err != nil {
        log.Fatal(err)
    }

    // Convert []byte to string and print to screen
    text := string(content)

    rgx := regexp.MustCompile(`<script type="text/javascript">([\s\S]*?)</script>`)
    // //buscamos todos los script en el html
    scripts := rgx.FindAllString(text, -1)

    delone := `<script>window.bsClickFunc=function(a,e,f){var b,c,d;Enabler?(b=f.match(/\d+/g),c=b?b.join(''):null,d='Click Exit'+(c?' '+c:''),a?Enabler.exitOverride(d,a):Enabler.exit(d)):window.bsOpenURL(a,e)}</script>`
    deltwo := "<script>window.bsOpenURL=function(a,b){a&&a.length>0&&window.open(a,b)}</script>"
    scriptMin := ""

    for _, script := range scripts {
        scriptMin = Minify(script)

        if scriptMin == delone {
            text = strings.Replace(text, script, scriptMin, 1)
            text = strings.Replace(text, delone, "", 1)

        }
        if scriptMin == deltwo {
            text = strings.Replace(text, script, scriptMin, 1)
            text = strings.Replace(text, deltwo, "", 1)

        }
    }

    bs := "<a href=\"javascript:window.open(window.clickTag)\" style=\"width: 100%; height: 100%; position: absolute; z-index: 9999;\"><img src=\"images/dclk.png\" border=0><div id=\"bs\"></div></a> "
    endhead := "<script type=\"text/javascript\"> var clickTag = \"http://www.google.com\"; </script> </head>"

    text = strings.Replace(text, "clickTag", "oldVar", -1)
    text = strings.Replace(text, "</head>", endhead, 1)
    text = strings.Replace(text, "<div id=\"bs\"></div>", bs, 1)

    err = os.Remove(index)
    if err != nil {
        log.Fatal(err)
    }

    f, err := os.OpenFile(index, os.O_WRONLY|os.O_CREATE, 0600)
    if err != nil {
        panic(err)
    }

    defer f.Close()

    if _, err := f.WriteString(text); err != nil {
        panic(err)
    }

}
